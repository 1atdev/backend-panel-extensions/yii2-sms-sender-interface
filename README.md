Yii 2 - SMS Sender Interface
=========================

[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](https://www.yiiframework.com/)
[![PHP Version Require](http://poser.pugx.org/phpunit/phpunit/require/php)](https://packagist.org/packages/phpunit/phpunit)

[![Packagist Version](https://img.shields.io/packagist/v/1atdev/yii2-sms-sender-interface.svg?style=flat-square)](https://packagist.org/packages/1atdev/yii2-sms-sender-interface)
[![Latest Stable Version](https://poser.pugx.org/1atdev/yii2-sms-sender-interface/version)](https://packagist.org/packages/1atdev/yii2-sms-sender-interface)
[![Total Downloads](https://poser.pugx.org/1atdev/yii2-sms-sender-interface/downloads)](https://packagist.org/packages/1atdev/yii2-sms-sender-interface)
![License](https://img.shields.io/packagist/l/1atdev/yii2-sms-sender-interface)

This package is based on [yetopen/yii2-sms-sender-interface](https://github.com/YetOpen/yii2-sms-sender-interface) but with minimum php version is `PHP v8.1`. Please read documentation
on [yetopen/yii2-sms-sender-interface](https://github.com/YetOpen/yii2-sms-sender-interface), as currently there is no anything new except different namespace and some of adjustment to use `PHP 8.1`.  

