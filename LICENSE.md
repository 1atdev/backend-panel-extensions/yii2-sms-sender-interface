# License

Copyright (C) Mirdani Handoko / IsAtDev

This software is available under licenses:

* GNU General Public License version 3 (GPLv3) as IsAtDev Community Edition

## GNU General Public License version 3 (GPLv3)

If you decide to choose the GPLv3 license, you must comply with the following terms:

This program is free software.
You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

* [GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007](https://www.gnu.org/licenses/gpl-3.0.html#license-text)


